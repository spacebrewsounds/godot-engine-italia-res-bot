from dotenv import load_dotenv
import os

load_dotenv(".env")

TOKEN = str(os.getenv("TOKEN"))

CHANNEL_ID = str(os.getenv("CHANNEL_ID"))

ADMIN_GROUP = str(os.getenv("ADMIN_GROUP"))

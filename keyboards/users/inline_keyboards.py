from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton

from keyboards.users import callback_datas

# User-side keyboard which shows up when the user sends a post
confirmation: InlineKeyboardMarkup = InlineKeyboardMarkup(row_width=2)

confirm_post = InlineKeyboardButton(text="Conferma", callback_data="confirm")
delete_post = InlineKeyboardButton(text="Cancella", callback_data="cancel")

confirmation.add(confirm_post, delete_post)


# Keyboard to be displayed in the admin group
admin_check_keyboards: InlineKeyboardMarkup = InlineKeyboardMarkup(row_width=2)

approve_post = InlineKeyboardButton(text="Approva", callback_data="approve")
reject_post = InlineKeyboardButton(text="Rifiuta", callback_data="reject")

admin_check_keyboards.add(approve_post, reject_post)


edit: InlineKeyboardMarkup = InlineKeyboardMarkup(
    [
        InlineKeyboardButton(
            "Titolo", callback_data=callback_datas.edit.new(state="title")
        ),
        InlineKeyboardButton(
            "Tags", callback_data=callback_datas.edit.new(state="tags")
        ),
        InlineKeyboardButton(
            "Immagine", callback_data=callback_datas.edit.new(state="photo")
        ),
    ],
    [
        InlineKeyboardButton(
            "Descrizione", callback_data=callback_datas.edit.new(state="description")
        ),
        InlineKeyboardButton(
            "Link", callback_data=callback_datas.edit.new(state="link")
        ),
    ],
)

#!/usr/bin/python3.8

import logging
import config
from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Regexp, AdminFilter, ChatTypeFilter
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.types import (
    ParseMode,
    ContentType,
    User,
    InlineKeyboardMarkup,
    InlineKeyboardButton,
)
from aiogram.utils import executor, markdown

from keyboards.users import inline_keyboards, callback_datas

logging.basicConfig(
    filename="telegram_bot_log.log",
    filemode="a",
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    datefmt="%H:%M:%S %d/%m/%Y",
    level=logging.INFO,
)

logger = logging.getLogger(__name__)
bot = Bot(token=config.TOKEN, parse_mode=ParseMode.HTML)

storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)


class Post(StatesGroup):
    title = State()
    photo = State()
    tags = State()
    description = State()
    link = State()
    confirm = State()
    cancel = State()


class Support(StatesGroup):
    support_request = State()


@dp.message_handler(
    ChatTypeFilter(types.ChatType.PRIVATE),
    commands="start",
)
async def start(message: types.Message):
    logger.info(f"User {message.from_user.full_name} is creating a new post.")

    await Post.title.set()
    await message.reply(
        "Ciao! Vuoi segnalare una risorsa utile alla community? Ti aiuterò io!"
        " Per cominciare inserisci il titolo della risorsa."
    )


@dp.message_handler(commands="start")
async def start_not_private(message: types.Message):
    await message.answer(
        "Per creare un nuovo post usa /start direttamente nella chat col bot."
    )


@dp.message_handler(content_types=ContentType.TEXT, state=Post.title)
async def process_title(message: types.Message, state: FSMContext):
    logger.info(
        f"User {message.from_user.full_name} sent a title for the resource: {message.text}."
    )
    await state.update_data(title=message.text)
    await Post.next()
    await message.reply(
        "Perfetto. Ora puoi mandarmi un'immagine riguardo alla risorsa "
        "oppure saltare questo passaggio scrivendo /skip."
    )


@dp.message_handler(state=Post.title)
async def process_title_invalid(message: types.Message, state: FSMContext):
    await message.reply("Manda un titolo in formato testuale.")


@dp.message_handler(commands="skip", state=Post.photo)
async def skip_photo(message: types.Message, state: FSMContext):
    logger.info(f"User {message.from_user.full_name} did not send a photo.")

    await state.update_data(photo=None)

    await message.reply(
        "Fantastico! Scrivi ora i tag che caratterizzano la risorsa. "
        "Sarà così più facile cercare tra risorse simili. "
        "I tag devono essere scritti nella forma: '#primotag #secondotag'."
    )
    await Post.next()


@dp.message_handler(content_types=ContentType.PHOTO, state=Post.photo)
async def process_photo(message: types.Message, state: FSMContext):
    logger.info(f"User {message.from_user.full_name} sent a photo.")

    await state.update_data(photo=message["photo"][-1].file_id)

    await bot.send_message(
        chat_id=message.chat.id,
        text="Fantastico! Scrivi ora i tag che caratterizzano la risorsa. "
        "Sarà così più facile cercare tra risorse simili. "
        "I tag devono essere scritti nella forma: '#primotag #secondotag'.",
    )
    await Post.next()


@dp.message_handler(state=Post.photo)
async def process_photo_invalid(message: types.Message, state: FSMContext):
    await message.reply("Manda un'immagine singola.")


@dp.message_handler(Regexp("(#\w+\s?)+"), state=Post.tags)
async def process_tags(message: types.Message, state: FSMContext):
    logger.info("Tags for the resource: %s", message.text)
    async with state.proxy() as data:
        data["tags"] = list(set(message.text.split()))
    await message.reply(
        "OK. Aggiungi ora una descrizione di "
        "poche righe di cos'è o cosa fa la risorsa."
    )
    await Post.next()


@dp.message_handler(content_types=ContentType.ANY, state=Post.tags)
async def process_tags_invalid(message: types.Message, state: FSMContext):
    await message.reply('Manda dei tag scritti in questo modo: "#primotag #secondotag"')


@dp.message_handler(content_types=ContentType.TEXT, state=Post.description)
async def process_description(message: types.Message, state: FSMContext):
    logger.info("Resource description: %s", message.text)
    async with state.proxy() as data:
        data["description"] = str(message.text)

    await message.reply(
        "Infine, scrivi il link a cui si può reperire la risorsa stessa."
    )
    await Post.next()


@dp.message_handler(state=Post.description)
async def process_description_invalid(message: types.Message, state: FSMContext):
    await message.reply("Manda una descrizione in formato testuale.")


@dp.message_handler(
    lambda message: any(entity.type == "url" for entity in message.entities),
    state=Post.link,
)
async def process_link(message: types.Message, state: FSMContext):
    await state.update_data(link=message.text)
    logger.info("Resource link: %s", message.text)
    await Post.next()

    await bot.send_message(
        chat_id=message.from_user.id,
        text="Grazie per il contributo! "
        "Per favore controlla che le informazioni"
        " date siano corrette.",
    )
    await generate_check_posts(
        user=message.from_user, state=state, recipient_chat=message.from_user.id
    )
    await bot.send_message(
        chat_id=message.from_user.id,
        text="Ecco il tuo post\! "
        "Premi i pulsanti qui sotto per __cancellare__ il post o "
        "__mandarlo__ in approvazione\! 😀",
        reply_markup=inline_keyboards.confirmation,
        parse_mode=ParseMode.MARKDOWN_V2,
    )


@dp.message_handler(state=Post.link)
async def process_link_invalid(message: types.Message, state: FSMContext):
    await message.reply('Manda un link nella forma "https://godotengineitalia.com".')


@dp.callback_query_handler(text_contains="confirm", state=Post.confirm)
async def process_user_confirm(
    query: types.CallbackQuery,
    state: FSMContext,
):
    await generate_check_posts(
        user=query.from_user,
        state=state,
        recipient_chat=config.ADMIN_GROUP,
    )
    logger.info(f"The user {query.from_user.full_name} submitted a post for approval.")
    await query.answer("Grazie! Il tuo post è stato sottoposto ai nostri admin.")
    await query.message.delete()
    await state.finish()


@dp.callback_query_handler(text_contains="cancel", state=Post.confirm)
async def process_user_cancel(query: types.CallbackQuery, state: FSMContext):
    await query.answer("Hai cancellato l'invio del tuo post.")
    await query.message.delete()  # TODO delete also the post message?
    logger.info(f"The user {query.from_user.full_name} deleted a post.")
    await state.reset_state()


@dp.callback_query_handler(
    AdminFilter(is_chat_admin=config.ADMIN_GROUP), text_contains="approve"
)
async def on_approved_post(query: types.CallbackQuery):
    if query.data.split(":")[1] == str(query.from_user.id):
        print("same user")
        await query.answer("Non puoi approvare il tuo post")
        return

    logger.info(
        f"The post by the user with ID {query.data.split(':')[1]} was approved by {query.from_user.full_name}."
    )

    message = query.message
    await message.delete_reply_markup()

    channel_message = await message.copy_to(chat_id=config.CHANNEL_ID)
    await message.reply(
        text=f"Il post è stato approvato da"
        f" {markdown.link(query.from_user.full_name, f'tg://user?id={query.from_user.id}')}",
        parse_mode=ParseMode.MARKDOWN_V2,
    )

    message_id = channel_message["message_id"]
    await bot.send_message(
        chat_id=query.data.split(":")[1],
        text=f"Congratulazioni\! Il tuo post è stato approvato ed è visibile nel canale a {markdown.link('questo', f't.me/c/{config.CHANNEL_ID[4:]}/{message_id}')} link\.",
        parse_mode=ParseMode.MARKDOWN_V2,
    )


@dp.callback_query_handler(
    AdminFilter(is_chat_admin=config.ADMIN_GROUP),
    text_contains="reject",
)
async def on_rejected_post(query: types.CallbackQuery):
    logger.info(
        f"The post by {query.data.split(':')[1]} was rejected by {query.from_user.full_name}"
    )
    await query.message.delete_reply_markup()
    await bot.send_message(
        chat_id=query.data.split(":")[1],
        text=f"Sono spiacente ma il tuo post è stato rifiutato. "
        f"Se pensi che ci sia stato un errore puoi contattare gli amministratori usando il comando /support.",
    )


@dp.message_handler(commands="support")
async def support_init(message: types.Message):
    await message.answer(
        "Grazie a questo comando puoi contattare gli amministratori. "
        "Se vuoi richiedere assistenza, segnalare un bug o dare un parere sul bot scrivi pure qui. "
        "Se vuoi cancellare l'azione corrente puoi sempre usare /cancel in qualunque momento."
    )
    await Support.support_request.set()


@dp.message_handler(state=Support.support_request)
async def support_send(message: types.Message, state: FSMContext):
    support_message = await message.forward(chat_id=config.ADMIN_GROUP)
    await support_message.reply(
        f"Richiesta di #supporto da parte di {message.from_user.full_name}"
    )
    await message.answer(
        "Grazie! Il tuo messaggio è stato appena inviato agli amministratori."
    )
    await state.reset_state()


@dp.message_handler(commands="help", state="*")
async def help_handler(message: types.Message):
    await message.answer(
        text=f"{markdown.bold('Godot Engine Italia Resource Manager Bot 📦')}\n\n"
        f"Il bot ha lo scopo di raccogliere in modo ordinato le risorse che i membri della community vogliono condividere a beneficio di tutti\. "
        f"Chiunque può sottoporre una risorsa\!\n\n"
        f"Al momento il bot richiede un {markdown.bold('titolo')}, dei {markdown.bold('tag')} inerenti alla risorsa,  un'{markdown.bold('immagine')} \(opzionale\), una breve {markdown.bold('descrizione')} e un {markdown.bold('link')} alla risorsa stessa\.\n\n"
        f"Le risorse sottoposte vengono moderate dagli amministratori della community e dopo l'approvazione sono pubblicate nel canale "
        f"{markdown.italic('Godot Engine Italia .RES')}\.\n\n"
        f"I comandi supportati dal bot sono:\n"
        f"/start \- avvia il bot e permette di creare un nuovo post\n"
        f"/cancel \- interrompe l'azione corrente\n"
        f"/help \- mostra questo messaggio\n"
        f"/support \- permette di contattare gli amministratori\n",
        parse_mode=ParseMode.MARKDOWN_V2,
    )


@dp.message_handler(
    ChatTypeFilter(types.ChatType.PRIVATE), commands="cancel", state="*"
)
async def cancel_action(message: types.Message, state: FSMContext):
    await message.reply("L'azione corrente è stata interrotta.")
    await state.reset_state()


async def compile_post(state: FSMContext, sender: User):
    # Using HTML formatting as the presence of unescaped hashtags is forbidden in Markdown
    async with state.proxy() as data:
        return (
            f'{markdown.hbold(data["title"])}\n\n{", ".join(data["tags"])}\n\n{data["description"]}'
            f'\n\nPuoi trovare la risorsa a {markdown.hlink("questo", data["link"])} link. '
            f"Per maggiori informazioni contattare "
            f'{markdown.hlink(f"{sender.full_name}", "tg://user?id={0}".format(sender.id))}.'
        )


async def generate_check_posts(user: User, state: FSMContext, recipient_chat: int):
    post_text = await compile_post(state=state, sender=user)

    keyboard = []
    if recipient_chat == config.ADMIN_GROUP:
        keyboard = [
            [
                InlineKeyboardButton(
                    "Approva", callback_data=callback_datas.approve.new(user_id=user.id)
                ),
                InlineKeyboardButton(
                    "Rifiuta", callback_data=callback_datas.reject.new(user_id=user.id)
                ),
            ],
        ]

    data = await state.get_data()
    if data.get("photo") is None:
        post_message = await bot.send_message(
            chat_id=recipient_chat,
            text=post_text,
            parse_mode=ParseMode.HTML,
            reply_markup=InlineKeyboardMarkup(row_width=2, inline_keyboard=keyboard),
        )
    else:
        post_message = await bot.send_photo(
            chat_id=recipient_chat,
            photo=data.get("photo"),
            caption=post_text,
            parse_mode=ParseMode.HTML,
            reply_markup=InlineKeyboardMarkup(row_width=2, inline_keyboard=keyboard),
        )
    return post_message


async def startup_notify(dp):
    message = "Il bot è online ✅"
    await bot.send_message(config.ADMIN_GROUP, text=message)
    await bot.send_message(config.CHANNEL_ID, text=message)


async def shutdown_notify(dp):
    message = "Il bot è offline ❌"
    await bot.send_message(config.ADMIN_GROUP, text=message)
    await bot.send_message(config.CHANNEL_ID, text=message)


if __name__ == "__main__":
    executor.start_polling(
        dp,
        skip_updates=True,
        on_startup=startup_notify,
        on_shutdown=shutdown_notify,
    )
